import '@babel/polyfill/noConflict';
import server from './server';

server.start(({ port }) => {
    console.log('The server is up!');
})
