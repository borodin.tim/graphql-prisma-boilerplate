import jwt from 'jsonwebtoken';

const generateToken = (id) => {
    return jwt.sign({ userId: id }, process.env.SECRET_KEY_JWT, { expiresIn: '30d' })
}

export default generateToken;